#!/usr/bin/env node
var version="2.290529.1" // FIX 

var cluster = require('cluster');
if (cluster.isMaster) {
  var restart = false;
  var http = require('http').createServer(handler); 
  var fs = require('fs'); 
  var i2caddr = '0';
  var io = require('socket.io')(http) 
  var mode=0;
  var slevel = 0;
  var booted = false;
  var fdevice = false;
  var xalg = 1;
  var { exec } = require('child_process');
  exec('/usr/bin/killall limesdr_debug -9',(err, stdout, stderr) => {
    exec('/home/pi/nrfscan/limesdr_stopchannel',(err, stdout, stderr) => {});
  });


/*
сделать инит i2c
  exec('/usr/sbin/i2cget -y 1 0x22 0 ',(err, stdout, stderr) => {
    if (!err) {i2caddr = '22'; fdevice=true;} 
    else {
     exec('/usr/sbin/i2cget -y 1 0x20 0 ',(err, stdout, stderr) => {
       if (!err) {i2caddr = '20'; fdevice=true;} else  {
         debug("No attenuation FOUND (0x20,0x21)!!! Check I2C connection");
       }
      });
    }
    if (fdevice) { console.log('i2c address :'+i2caddr);}
  });

*/

  http.listen(8888);
//  var ina219 = require('ina219');
//  ina219.init();
//  ina219.enableLogging(true);
  var worker = cluster.fork();
  cluster.on('exit', function(worker, code, signal) {worker = cluster.fork();});
  function debug(data) {
    console.log(">>"+data);
  }
  var xstarted = false;
  var fix=0;

  var xdelay = parseInt(process.argv[2]);
  if (xdelay<1 || xdelay>1000) xdelay=20;
  io.sockets.on('connection', function (socket) {
    socket.on('disconnect', function () {
      debug("Disconnect");
      console.log("-----------------------------------------------");
      started = false;
      worker.send({cmd:'stop'});
    });
    var maxzz = Array();
    var counter = 0;
    var xmax = 0;
    var xhr = 0;
    var xdbm = 0;
    var xz = 0;
    var xpr = 0;
    var xst = 0;
    var xlevel = 255;
    var xxtime = Date.now();
    var sxtime = Date.now();
    var started = false;
    var levels = Array();
    var oldi2c = 135;
    for (i=247;i<256;i++) levels[i] =  0;
    for (i=127;i<136;i++) levels[i] =  0;
    for (i=119;i<128;i++) levels[i] =  0;
    var xfilter = 8;
    var ofilter = 8;
    var filters = {1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8}; // filters --
    var xxdbm = 0;
    var xxhr = 0;
    var xxxdbm = 0;
    var xxxhr = 0;
    var lna = 0,olna=0;
    var one = 0;
    var startedbin = false;
    var xxbufferr = 0;

    if (!xstarted) {
      xstarted = true;
      var timerId = setInterval(function() {
//        const { exec } = require('child_process');
        if (started) {



	if (startedbin) {
	fs.readFile('/dev/shm/iq','hex', function(err,data) {
	c = new Buffer.from(data,'hex');
	mustsize=xxbufferr*4;
	if (c.length!=mustsize) return;
    	socket.emit('c',c);
    	console.log(">>Sending block : "+mustsize+" bytes");
    });
   }




          var trudata = true;
          if (Date.now()-sxtime>1000) {trudata = false;}
// 20.02         if (xhr==0 || !trudata) return;
          if (!trudata) return;
//          outdbm = (parseInt(xdbm)-levels[xlevel]);
          outdbm = parseInt(xdbm);

          sf = false;
	  lna = lna*1;
//	  if (lna!=olna) {fix=8;}
          if (fix) { 
            fix--;
            outdbm = xxxdbm;
            xhr = xxxhr;
//	    console.log("::::"+fix);		
//return;
          } else {
            xxxdbm = xxdbm;
            xxdbm = outdbm;
            xxxhr = xxhr;
            xxhr = xhr;
          }
// 20.02         if (xhr==0 || !trudata) return;
          if (!trudata) return;
	  if (alg!=5) {
          socket.emit('b',[xhr,outdbm*100,lna,levels[xlevel],xfilter,xlevel]);
//	  lna = trim(lna);
          xst++;
	  olna = lna;
          if ((xst>25 && alg!=2) || (xst>2)) { 
            debug('>>FIX:'+fix+':'+oldi2c+':LNA:'+lna+':Filt:'+xfilter+":ATT:"+xlevel+":"+levels[xlevel]+">"+outdbm+", "+xhr);
//            exec('echo "'+one+'" > /sys/class/gpio/gpio21/value',(err, stdout, stderr) => {});
            if (one) one = 0; else one = 1;
            xst = 0;
          }
	  }
        }
      },20);

    }
    var oxlevel = 0;
    function i2cset(data) {
    return 0;
      if (!fdevice) { xlevel=data;xxtime = Date.now();return 0;}
      if (data==oldi2c) return data;
      if (data=='init') xdata = ' 0 0'; else {
        xdata = ' 9 '+data;
        xlevel = data;
        oxlevel = data;
        fix=20;
        debug("!i2c_set>>:"+i2caddr+":"+data+">>>"+levels[oldi2c]);
      }
      oldi2c = xlevel;
      const { exec } = require('child_process');
      exec('/usr/sbin/i2cset -y 1 0x'+i2caddr+xdata,(err, stdout, stderr) => {
        if (err) return;
        xlevel = data;
        oxlevel = data;
        debug("i2c_set>>:"+i2caddr+":"+data+": LV:"+levels[xlevel]);
        xxtime = Date.now();
    //    fix=10;
      });
      return data;
    };
    function i2cget() {
return 0;
      if (!fdevice) return 0;
      const { exec } = require('child_process');
      exec('/usr/sbin/i2cget -y 1 0x'+i2caddr+' 9',(err, stdout, stderr) => {
        if (err) return;
        var hex = stdout;
        socket.emit('update',['att',hex]);
        debug("i2c_get:"+hex);
      });
    }
//    i2cset(255);i2cget();
    var auto = false;
    if (process.argv.slice(2)=='restart') isrestart = true; 
    else  isrestart = false;
    socket.on('ext', function(data) { 
      if (data[0]=='auto') {
        if (data[1]==true) auto = false; else auto = true;
        debug("Set Auto:"+auto);
        worker.send({cmd:'auto',data:auto});
      }
      if (data[0]=="att") {
        attnow = data[1];
        worker.send({cmd:'attlevel',data:attlevel});	
        return;
      }
      if (data[0]=="attm") {
        attmin=data[1][0];
        attmax=data[1][1];
        return;
      }
      if (data[0]=='mlevel') level = parseInt(data[1]);
      if (data[0]=='xblock') xblock = parseInt(data[1]);
    });

    socket.on('setalg', function(xdata) {
	console.log('LISA-------------------------------------------------------------------- SET_ALG:'+xdata);
      xalg=xdata;alg=xdata;worker.send({cmd:'setalg',data:xdata});
    });
    socket.on('gethwstatus', function() { 
    console.log('LISA-------------------------------------------------------------------- GETHWS:');
      const { exec } = require('child_process');
      exec('cat /sys/class/thermal/thermal_zone0/temp',(err, stdout, stderr) => {
        if (err) return;
        var temp = stdout;
        socket.emit('temp',temp);
      });
    });
    socket.on('starts', function(data) {
    console.log('LISA-------------------------------------------------------------------- STARTS');
      if (data==' START ' && started) return;
      xstarted = false;
      if (data!=' START ') {	started = false;worker.send({cmd:'stop'});} 
      else {debug("start!!!");started = false;worker.send({cmd:'start'});}
    });

    socket.on('setbuf', function(data) {
	console.log('-------------------------------------------------------------------- SET_BUF:'+data);
      worker.send({cmd:'setbuf',data:data})
    });


    socket.on('setzone', function(data) {
	console.log('-------------------------------------------------------------------- SET_zone:'+data);
      worker.send({cmd:'setzone',data:data})
    });

    socket.on('setfreq', function(data) {
	console.log('-------------------------------------------------------------------- SET_FREQ:'+data);
      worker.send({cmd:'setfreq',data:data})
    });
    socket.on('setflt', function(data) {
	console.log('-------------------------------------------------------------------- SET_FLT:'+data);
      ofilter=xfilter;xfilter = data; debug("Filter : "+data);
      xdiff = filters[ofilter]-filters[xfilter];
      xlevel-=xdiff;
    });
    socket.on('xboost', function(data) {
      worker.send({cmd:'setgain',data:data})
    });
    socket.on('setsr', function(data) {	console.log('-------------------------------------------------------------------- SET_SR:'+data);worker.send({cmd:'setsr',data:data})});
    socket.on("params", function(data) {
	console.log('-------------------------------------------------------------------- SET_PARAMS:'+data);
      mode = 0; worker.send({cmd:'params',data:data})
    });
    var oldatt = attnow;
    var ox = 0;
    var oh = 0;
    var sxdelay = 0;
    var pichok = 0;
    worker.on('message', function(msg) {
      switch(msg.cmd) {
	case 'xx':
		started = msg.data;
	break;
	case 'xbuf':
	xxbufferr = msg.data;
	startedbin=true;
        console.log("<< STARTEDBIN!!!");
	break;
        case 'restart': 
          if (!started) {
            started = true;
            isrestart=false; 
            worker.send({cmd:'start'});
          };
        break;
        case 'xalg' : 
          socket.emit('update',['attx',msg.data]);
          console.log('Start L');
          if (!started) {
//            started=true;
//            worker.send({cmd:'start'});
          };
        break; // att for client
        case  'xatt' :
          debug("::>>>>>"+msg.data+"::::::::::::"+xlevel);
          oatt = xlevel;
          break;
        case  'i2c': 
        break;
        case    'b': 

          ox = xhr;
          oh = xdbm;
          olna = lna;
	 sxtime = Date.now();

            xhr = msg.h; 
            xdbm = msg.data;
            lna = msg.lna;

/*
          var sxlevel = xlevel;
          zdbm = parseInt(xdbm)-levels[xlevel];
          if (xlevel>119 && xlevel<128) {
            if (zdbm>=-30) {sxlevel = 247 + filters[xfilter];}
          } 
          else if (xlevel>127 && xlevel<136) {
            if (zdbm<=-2) { sxlevel = 247 + filters[xfilter];}
          }	
          else if (xlevel>247 && xlevel<256) {
            if (zdbm<-30 && lna>=48) {sxlevel = 119 + filters[xfilter];}
            if (zdbm>-2 && xalg!=3 && !lna) {sxlevel = 127 + filters[xfilter];}
          }	
          if ((sxlevel!=xlevel)) {
            sxdelay++; 
            if (sxdelay>7) {
              console.log('>>>'+sxlevel+':'+filters[xfilter]+":"+xfilter);
              sxdelay=0;
              i2cset(sxlevel);
            }
          } else {sxdelay=0;}
*/
        break;
        case 'stop':
          socket.emit('update',['xs',1]);
          started = false;
	  startedbin=false;
        break;		
      }//switch end
    }); //worker.on end
  }); //io.socket.om CONNECTION - end
  function handler (req, res) { //create server
    var url = "";
    if (req.url=='/') url="index.html"; url = req.url+url;
    if (req.url=="/")debug("Got client!");
//    debug(">>"+i2caddr+":"+fdevice);
    fs.readFile(__dirname + '/public'+url, function(err, data) { //read file index.html in public folder
      if (err) {
        res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
        return res.end("404 Not Found");
      }
      ext = url.substring(url.lastIndexOf('.')+1);
      var contt = "text/html";
      switch(ext) {
        case "js" :  contt = "text/javascript";break;
        case "css" : contt = "text/css";break;
      }
      res.writeHead(200, {'Content-Type': contt}); //write HTML
      res.write(data); //write data from index.html
      return res.end();
    });
  }
}
//----------------------------------------------------------------------- FORKED
if (cluster.isWorker) {
  function dif(a,b,c=0.1){
    if (Math.abs((a/b)-1) < c) 
    return true;
    else
    return false; 
  }
  function debug(data) {
    tm = Date.now().toString();
    tm = tm.substr(0,10)+'.'+tm.substr(10);
    console.log(tm+":"+data);
  }  
  function parse (f) {
    if (typeof f === 'number') return f
    f = f.replace(/M/i, '* 1000000')
    f = f.replace(/k/i, '* 1000')
    f = f.replace(/hz/, '')
    return eval('(' + f + ')'); // yolo
  }
  var startf = false
  var maxiq = 0
  var pont = 0
  var started = false;
  var attlevel = 0;
  //var nsamples = 1000
  var level = 10                   // Min level data block of (xblock) samples
  var xblock = 1000		 // Number of samples in block
  var lime_freq = 898000000;
  var lime_sr = 5000000;
  var xgain = 0;
  var xbuffer = 5000;
  var attmin=20;			 // Minimum AUTO LEVEL EXTERNAL LNA
  var attmax=140;			 // Maximum AUTO LEVEL EXTERNAL LNA
  var mcp = 0;
  var auto = true;
  var attnow = 128;
  //var extlna=0;
  var mode = 0;
  var alg = 1;
  var isgood = true;
  var skipped = 0;
  var coff = 0;
  var levels = Array();
  for (i=247;i<256;i++) levels[i] = 0;
  for (i=127;i<136;i++) levels[i] = 0;
  for (i=119;i<128;i++) levels[i] = 0;
  var xa = 0;
  var sxd = 0;
  var zone = 3;
//  if (alg==5) xbuffer=5000;

  function xsend(a,dbm,lna) {
    process.send({cmd:'b',h:a,data:dbm,lna:lna});
    xa = a;
    sxd = dbm;
  }
  var laststate = 0;
  var cmd, spawn, spawn = require("child_process").spawn;
  function starts() {
//if (cmd) cmd.kill("SIGINT");
	started = false;
	startedbin = false;

	process.send({cmd:'xx',data:false});
    if (!lime_freq) lime_freq=104000000;
    console.log(">Start : FRQ:"+lime_freq+' ALG :'+alg+' SR :'+lime_sr+' B:'+xbuffer+' G:'+xgain+'Zone:'+zone);
    console.log('/home/pi/limescan/limesdr_debug', ['-f',lime_freq,'-m',alg,'-n', -1, '-l',xbuffer,'-z',zone]);

    cmd = spawn('/home/pi/limescan/limesdr_debug', ['-f',lime_freq,'-m',alg,'-n', -1, '-l',xbuffer,'-z',zone], {});
    cmd.on("exit", function(code, signal) {
      console.log("got shutdown:"+signal+':'+code);
      if (isrestart) {console.log('+++>>>');process.send({cmd:'restart'});}
	started = false;
	startedbin=false;
      return; // starts();
    });
    cmd.stderr.on("data", function(data) {
      if (!started) started = true;
	process.send({cmd:'xx',data:true})    
	sdata = data.toString();

	if (alg==5) {
	      if (sdata.substr(0,8)=="getready") {process.send({cmd:'xbuf',data:xbuffer});}
	    return;
	}
//      if (sdata.substr(0,8)=="getready") { process.send({cmd:'xbuf',data:xbuffer});}
      var xdata = parseInt(data);
/*
      if (sdata.substr(1,4)=='rans' || sdata.substr(1,4)=='RANS') {
        started = false;
        cmd.kill("SIGINT");
        debug("stopped !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        isrestart = true;
        return;
      }
*/
      if (isNaN(sdata.substr(0,1))) return;
      xdata = sdata.split(",");
      xsend(Number(xdata[0]),xdata[1],xdata[2]);
//      console.log("|||"+data+"|||");

    });

    cmd.stdout.on("data", function(data) {

      console.log(" OUT>"+data+"     "+data.length);


    });
  }
  var isrestart = false;
  process.on('message', function(msg) {
    switch(msg.cmd)  {
      case 	  'setalg': 
        alg = msg.data;
        console.log(">>"+alg); 
        process.send({cmd:'xalg',data:msg.data}); 
      break;
      case 	 'setfreq': 
        console.log(">>Set Freq:"+msg.data);
        lime_freq = msg.data;
      break;
      case	    'skip': 
        skipped = msg.data; 
        coff = msg.level; 
      break;
      case 	    'setbuf': xbuffer = msg.data; break;
      case 	    'setzone': 
	console.log(">>>-------------------SET ZONE:"+msg.data);
        zone = parseInt(msg.data);
	console.log("===",started);
	if (started && cmd.stdin) cmd.stdin.write((zone+1)+'\n');

	 break;
      case 	    'auto': auto = msg.data; break;
      case 	'attlevel': attlevel = msg.data; break;
      case 	   'setsr': {      sr = msg.data;}; break;
      case	'setblock': 
        if (msg.data>0) xblock = msg.data; 
        debug("SET BLOCK SIZE: "+msg.data);
      break;
      case     'setgain': 
        level = msg.data;debug("SET GAIN: "+msg.data);
        console.log(">> GAIN : ("+msg.data+")");
        cmd.stdin.write(msg.data);				    
      break;
      case      'params': 
        slna = msg.data[0];
        svga = msg.data[1]; 
        attmin = msg.data[5]; 
        attmax= msg.data[6];
        lime_freq = msg.data[2]; 
        lime_sr = msg.data[3];
        xblock = msg.data[4];
        debug("SET Gain[lna,vga,lime_freq,lime_sr]: "+msg.data);
      break;
      case	   'start':
        debug("Starting!");
	startbin = false;	
        starts();
      break;
      case 	    'stop':
        isgood = false;
        const { exec } = require('child_process');
        exec('/usr/bin/killall limesdr_debug',(err, stdout, stderr) => {
          debug("stopped");
	  started = false; 
          process.send({cmd:'stop'});		    
        });
      break;
    }
  });
}
