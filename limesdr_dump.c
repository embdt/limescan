/*version 2.*/
#define MY_VERSION "v2.2305.23.1"
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <lime/LimeSuite.h>
#include "wiringPi.h"
#include "wiringPiI2C.h"
#define INIFILENAME "./limesdr_debug.init"
#define i2c_adr 0x20
#define i2c_delay 20000
///////////////////////////////////
char version[16]=MY_VERSION;
int debug=0;
//int dumpcnt=0;
clock_t start, end;
clock_t sstart, send;
double cpu_time_used;
//sstart=clock();
struct inis { //for init-file
  char name[25];
  double value;
};
struct inis iniar[40];
char tmpstr[70];
char *ptmp;
FILE* fd ;
int stringcnt=0;
int flagzone=0;
short ourpacket=0;
double lastvalue=0;
  int curzone=5;
int parstr(char instr[],int idx){ //parsing string in two part
  char first[25];
  char second[25];  
  int i1=0;
  int i2=0;
  char valuetmp; 
  char* endstr;
  while (instr[i1]!='=') i1++;
  strncpy (first, instr, i1);
  first[i1]='\0';
  strncpy (second, &instr[i1+1], 25);
  while (second[i2]!='/'  && second[i2]!='\0' && second[i2]!=' ') i2++;
  if (second[i2]!='\0') second[i2]='\0';
  strcpy(iniar[stringcnt].name, first);
  iniar[stringcnt].value = strtod(second,&endstr);
}
int ini2arr(){ //add init-file string 2 array
  int i=0;
  stringcnt =0;
  fd = fopen( INIFILENAME, "r+b" );
  if ( fd == NULL ) perror("fopen()");
  else while (!feof(fd)){
    ptmp=fgets(tmpstr,70,fd);
    if (tmpstr[0]!='#' && 
        tmpstr[0]!=' ' && 
        tmpstr[0]!='\0' && 
        tmpstr[0]!='\n' && 
        tmpstr[0]!='\r') {
      parstr(tmpstr,stringcnt); 
      stringcnt++;
    }
  }
  fclose(fd);
  return stringcnt;
}
int doublret(char instr[]){ //return Value of name
  int i=0;
  while (strcmp(instr,iniar[i].name)!=0 && i<stringcnt) i++;  
  if (strcmp(instr,iniar[i].name)==0) {lastvalue=iniar[i].value; return 0;} 
  else return -1;
}
///////////////////////////////////
int main(int argc, char** argv)
{
  float snr_global;
  int alg = 1;
  int i=0;
  int isAlexIni = 0;
  int nowgain= 0;
  int prevgain= 0;
  unsigned long xmblock = 0;
  unsigned long int prevxmblock=0;
  int oldblock = 0;
  int xskip = 0;
  int ii= 0;
  int sbl=0;//,xsbl=0;
  float bell;
  int nb_samples=0;
  int band=0;
  double gain = 0;
  unsigned int freq = 940000000;
  double bandwidth_calibrating = 2500000;
  double sample_rate = 1400000;
  lms_device_t* device = NULL;
  double host_sample_rate;
  unsigned int buffer_size = 1000*50;
  unsigned int device_i = 0;
  unsigned int channel = 0;
  size_t antenna = 3; //  1-RX LNA_H port   2-RX LNA_L port  3-RX LNA_W port
  char* output_filename = NULL;

  int signalthreshold_m1=12; //   M1   
  int blocksize=1024;     //   M1    size of block;
  long int curpwr=0;      //   M1
  int maxpwr=0;           //   M1
  int blockcnt=0;         //   M1
  double sr2=480000;       //   M2
  int signalthreshold=12;  //   M2   
  int skrblock=16;         //   M2  size of block 
  int oursize=17;           //   M2    
  int deltablock=2;        //   M2
  unsigned long int tmpiq; //       //   M2    
  int jj=0;                //   M2    
// new m3
  int m3signalthreshold=12;  //   M2   
  int m3skrblock=16;         //   M2  size of block 
  int m3oursize=16;           //   M2    
  int m3deltablock=2;        //   M2
  double m3dif=0.1;
  double m2dif=0.25;
//  double sr3=5000000;           //  M3  
  int debugcnt=0;               //  M3  
  long long int maxnumber = 0;       //  M3 
  long int maxcount = 0;        //  M3 
  int zoom = 1;                 //  M3 
  int periodsize = round(sample_rate/10000*25);//2.5milesec
  int subperiod=1250;           //  M3 250microsec=1250smpl
/////////////////////////////////////// from    ini
  int maxvar=0;
  maxvar=ini2arr(); 
  if (maxvar>0){ //if init-file present:
//   if (debug) printf("vsego=%d\n",maxvar);
//    while (i<stringcnt) {
//printf("name='%s' value='%f'\n",iniar[i].name,iniar[i].value);i++;}  
//anywhere
    if (doublret("isAlexIni")==0) isAlexIni=lastvalue;
    if (doublret("sample_rate")==0) sample_rate=lastvalue;
    if (doublret("buffer_size")==0) buffer_size=lastvalue;
//met.1
    if (doublret("signalthreshold_m1")==0) signalthreshold_m1=lastvalue; //old
    if (doublret("blocksize")==0) blocksize=lastvalue;                   //old?  
//met.2    
    if (doublret("sr2")==0) sr2=lastvalue;                        
    if (doublret("signalthreshold")==0) signalthreshold_m1=lastvalue;    //old?
    if (doublret("skrblock")==0)  skrblock=lastvalue;                        //? //old
    if (doublret("oursize")==0)   oursize=lastvalue;
    if (doublret("deltablock")==0) deltablock=lastvalue;
    if (doublret("m2dif")==0) m2dif=lastvalue;                      //old
//met.3
    if (doublret("m3signalthreshold")==0) m3signalthreshold=lastvalue;        //old
    if (doublret("m3skrblock")==0)  m3skrblock=lastvalue;       //old
    if (doublret("m3oursize")==0)   m3oursize=lastvalue;        //old
    if (doublret("m3deltablock")==0) m3deltablock=lastvalue;    //old
    if (doublret("m3dif")==0) m3dif=lastvalue;                  //old
    if (doublret("overalg")==0)  alg=lastvalue;                 //old
  }
//////////////////////////////////////// from command prompt:
  if ( argc < 2 ) {
   printf("--usage: %s <OPTIONS>\n", argv[0]);
   printf("  -f <FREQUENCY>\n"
      "  -b <BAND filter> (default: bypass)\n"
//      "  -g <GAIN_dB> range [0, 73] (default: 0)\n"
      "  -l <BUFFER_SIZE>  (default: %d)\n",buffer_size,
      "  -d <DEVICE_INDEX> (default: 0)\n"
      "  -o <OUTPUT_FILENAME> (default: stdout)\n"
      "  -n NUMBER of sampes\n"
      "  -m method default 1 (0-16bit IQ-stream,1,2,3-alg1,2,3)\n" 
      "  -z zone N 0..5"
      "  -v Curent version\n"  
    );
    return 1;
  }
  for ( i = 1; i < argc-1; i += 2 ) {
    if      (strcmp(argv[i], "-f") == 0) { freq = atof( argv[i+1] ); }
    else if (strcmp(argv[i], "-b") == 0) { band = atoi( argv[i+1] ); }
    else if (strcmp(argv[i], "-z") == 0) { curzone = atof( argv[i+1]); }
    else if (strcmp(argv[i], "-l") == 0) { buffer_size = atoi( argv[i+1] ); }
    else if (strcmp(argv[i], "-d") == 0) { device_i = atoi( argv[i+1] ); }
    else if (strcmp(argv[i], "-o") == 0) { output_filename = argv[i+1]; }
    else if (strcmp(argv[i], "-m") == 0) { alg = atof( argv[i+1] ); }
    else if (strcmp(argv[i], "-v") == 0) { printf("%s\n",version);return 1; }
    else if (strcmp(argv[i], "-n") == 0) { maxnumber = atof( argv[i+1] ); }
  }
  if ( freq == 0 ) {
    fprintf( stderr, "ERROR: invalid frequency : %d\n", freq );
    return 1;
  }
  if (alg==2 || alg==4) {
    buffer_size=16000;
    sample_rate = sr2;
  }
/////////////////////////////////////////////////////////////////////////////////////////////////
  struct s16iq_sample_s {
    short i;
    short q;
  } __attribute__((packed));
  struct s16iq_sample_s *buff = (struct s16iq_sample_s*)malloc(sizeof(struct s16iq_sample_s) * 300000);
  if ( buff == NULL ) {
    perror("malloc()");
    return 1;
  }
  FILE* fd;
  if ( output_filename != NULL ) {
    fd = fopen( output_filename, "w+b" );
    if ( fd == NULL ) {
      perror("fopen()");
      return 1;
    }
  } else fd = stderr;
/////////////////////////////// function of limeutils
  lms_range_t curbwr;
  float_type hosthz;
  float_type rfhz;
  float_type curbw;
  unsigned int defreg;
  int getlna(){
    uint16_t lnag;
    if ( LMS_ReadParam(device,LMS7param(G_LNA_RFE),&lnag) < 0 ) {
      fprintf(stderr, "LMS_ReadParam() lna: %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else return lnag;
  }
  int gettia(){
    uint16_t tiag;
    if ( LMS_ReadParam(device,LMS7param(G_TIA_RFE),&tiag) < 0 ) {
      fprintf(stderr, "LMS_ReadParam() tia : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else return tiag;
  }
  int getpga(){
    uint16_t pgag;
    if ( LMS_ReadParam(device,LMS7param(G_PGA_RBB),&pgag) < 0 ) {
      fprintf(stderr, "LMS_ReadParam() pga : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else return pgag;
  }
  long int getbwr(){ //return min bandwidth & out 2 term min & max 
    if ( LMS_GetLPFBWRange( device, 0, &curbwr ) < 0 ) {
      fprintf(stderr, "LMS_GetLPFBWRange() : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else {
      printf ("Curent Bandwidth range  min=%.1lfMHz max=%.1lfMHz\n",curbwr.min/1000000,curbwr.max/1000000);
      return round(curbwr.min);
    }
  }
  int getsr(){ // return SampleRate KHz & out 2 term Host&RF-rate
    if ( LMS_GetSampleRate( device, 0 , 0, &hosthz, &rfhz ) < 0 ) {
      fprintf(stderr, "LMS_GetSampleRate() : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else {
     if (debug) printf("Host=%2.1fMHz (%lf) RF=%2.1fMHz(%lf)\n",round(hosthz/1000)/1000,hosthz,round(rfhz/1000)/1000,rfhz);
      return round(hosthz/1000);
    }
  }
  int setsr(double srate){ //set samplerate in Hz & out 2 term Host&RF-rate
    if ( LMS_SetSampleRate( device, srate, 0 ) < 0 ) {
      fprintf(stderr, "LMS_SetSampleRate() : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else {
      if ( LMS_GetSampleRate( device, 0 , 0, &hosthz, &rfhz ) < 0 ) {
        fprintf(stderr, "LMS_GetSampleRate() : %s\n", LMS_GetLastErrorMessage());
        return -1;
      } else {
       if (debug) printf("Host=%5.3fMHz RF=%lf\n",round(hosthz/1000)/1000,rfhz);
        return 0; 
      }
    }
  }
  int setgain(int db){ //set gain in DB & return gain
    if ( db >= 0 ) {
    if (debug) printf("set gain@%ddB",db);
      if ( LMS_SetGaindB( device, 0, 0, db ) < 0 ) {
        fprintf(stderr, "LMS_SetGaindB() : %s\n", LMS_GetLastErrorMessage());
        return -1;
      } else  return db;  
    } else return -1;
  }
  int getgain(){ //return gain in dB
    unsigned curgain;
    if ( LMS_GetGaindB( device, 0, 0, &curgain ) < 0 ) {
      fprintf(stderr, "LMS_GetGaindB() : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else  return round(curgain); 
  }
  long int getbw(){ //get bedwidth
    if ( LMS_GetLPFBW( device, 0, 0, &curbw ) < 0 ) {
      fprintf(stderr, "LMS_GetLPFBW() : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else {
      printf ("Curent Bandwidth = %2.3lfMHz\n",curbw/1000000);
      return round(curbw);
    } 
  }
  int setbw(float_type newbw){ //set bandwith rate
    if ( LMS_SetLPFBW( device, 0, 0, newbw ) < 0 ) {
      fprintf(stderr, "LMS_SetLPFBW() : %s\n", LMS_GetLastErrorMessage());
      return -1;
    } else {
      printf ("New Bandwidth = %2.3lfMHz\n",newbw/1000000);
      return 0;
    } 
  }
  int setminbw(){ //set minbandwith
    if ( LMS_SetLPFBW( device, 0, 0, 1400000 ) < 0 ) {
      fprintf(stderr, "LMS_SetLPFBW() : %s\n", LMS_GetLastErrorMessage());
       return -1;
    } else {
      getbw();
      return 0;
    }
  }
////////////////////////////////        INIT LIME       /////////////////
  int device_count = LMS_GetDeviceList(NULL);
  void zerror(int retcode){
    if (retcode<0) fprintf(stderr, "LMS ERROR:%s\n", LMS_GetLastErrorMessage());
  }
  lms_info_str_t device_list[ device_count ];                    ///STEP 1
  zerror(LMS_GetDeviceList(device_list));                        ///STEP 2
  if (debug) printf(">device list recived?\n");
  zerror( LMS_Open(&device, device_list[ device_i ], NULL));     ///STEP 3
  if (debug) printf(">device pointer returned?\n>RESET!..\n");
  zerror(LMS_Reset(device)); //   reset                          ///STEP 4
  if (debug) printf(">Seting DEFAULT...\n");
  zerror(LMS_Init(device));  //  default                         ///STEP 5
  zerror(LMS_EnableChannel(device, LMS_CH_RX, 0, true));//RX-0   ///STEP 6
  if (debug) printf(">Seting channel to RX-0\n");              
  if (alg==2 || alg==4) sample_rate=sr2;
  zerror(LMS_SetSampleRate(device, sample_rate, 0));             ///STEP 7
  if (debug) printf(">Seting samplerate =%9.0f done!\n",sample_rate);
  // LMS_GetSampleRate(device, LMS_CH_RX, 0, &host_sample_rate, NULL );
//////// set antenna pams  1=RX LNA_H 2=RX LNA_L 3=RX LNA_W    
  zerror(LMS_SetAntenna( device, LMS_CH_RX, 0, 3 ));/////        ///STEP 8
  if (debug) printf(">Seting antenna. done!\n");
  if ((alg==2 || alg==4 ) & isAlexIni) {
    printf(">>try Alex config load:\n");
    zerror(LMS_LoadConfig(device,"./lowbw.ini"));            // STEP 8-A
  } else isAlexIni=0;
  zerror(LMS_Calibrate(device, LMS_CH_RX, 0,bandwidth_calibrating,0));///STEP 9
  if (debug) printf(">Calibrating. done!\n");
  zerror(LMS_SetLOFrequency( device, LMS_CH_RX, 0, freq));      ///STEP 10
  if (debug) printf(">Seting freq =  %d done!\n",freq);
  zerror(LMS_SetGaindB( device, LMS_CH_RX, 0, gain ));          ///STEP 11
  if (debug) printf(">Set gain to %2.0f done!\n",gain);
///////////// init & start stream
	lms_stream_t rx_stream = {
		.channel = channel,
		.fifoSize = buffer_size * sizeof(*buff),
		.throughputVsLatency = 0.5,
		.isTx = LMS_CH_RX,
		.dataFmt = LMS_FMT_I16
	};
  if (debug) printf("setup stream:");
	if ( LMS_SetupStream(device, &rx_stream) < 0 ) {
		fprintf(stderr, "LMS_SetupStream() : %s\n", LMS_GetLastErrorMessage());
		return 1;
	}	
  if (debug) printf("ok\nstart stream:");
	LMS_StartStream(&rx_stream);
  if (debug) printf("ok.\n");
////////////////////////////// our functional ///////////////////////////////
//////// i2c
  union i2c_smbus_data{
    uint8_t  byte ;
    uint16_t word ;
    uint8_t  block [34] ;// block [0] is used for length + one more for PEC
  };
  int devlna=wiringPiI2CSetup(i2c_adr);
  usleep(i2c_delay*2);
  if (debug) printf("main.I2C init device ok.\n"); 
  wiringPiI2CWriteReg8 (devlna, 0, 0 );usleep(i2c_delay*4);
  wiringPiI2CWriteReg8 (devlna, 9, 255 );usleep(i2c_delay); defreg = 255;
  if (debug) printf("main.I2C registr init ok.\n");
///// LNA on/off 
  int extlnaon(){
      int reg=wiringPiI2CReadReg8(devlna,9);usleep(i2c_delay);
      if (reg>127) wiringPiI2CWriteReg8(devlna,9, (reg-128));usleep(i2c_delay);
      if (debug) printf("    LNA  on.\n");
  }  // set @channal lna=ON +16dB
  int extlnaoff(){
      int reg=wiringPiI2CReadReg8(devlna,9);usleep(i2c_delay);
      if (reg<=127) wiringPiI2CWriteReg8(devlna,9,(reg+128));usleep(i2c_delay);
      if (debug) printf("    LNA  off.\n");
  } // set  @channal lna=OFF +0dB
//////   ATT on/off
  int extatton(){
      int reg=wiringPiI2CReadReg8(devlna,9);usleep(i2c_delay);
      wiringPiI2CWriteReg8 (devlna, 9, (reg&135));usleep(i2c_delay);
      if (debug) printf("    ATT on. -16dB.\n");
  } // set  @channal att -16dB
  int extattoff(){
      int reg=wiringPiI2CReadReg8(devlna,9);usleep(i2c_delay);
      wiringPiI2CWriteReg8 (devlna, 9, ( (reg&135)+120 ));usleep(i2c_delay);
  if (debug) printf("    ATT off  0dB.\n");
  } // set  @channal att -0dB
////////  BPF/BAND
  int setfilter(int n){
    if (n<1 || n>8) n=8;
    int reg=wiringPiI2CReadReg8(devlna,9);usleep(i2c_delay);
    int bpf[9]={0,3,1,2,0,4,6,5,7};  //array of bytes for i2c reg
    wiringPiI2CWriteReg8 (devlna, 9, ((reg&248) + bpf[n] ) );usleep(i2c_delay);
    return ((reg&248) + bpf[n] ); // 
  }
  int setband(int band){
    int reg=wiringPiI2CReadReg8(devlna,9);usleep(i2c_delay);
    int bands[7]={3,5,20,28,8,7,1}; // array of availeble bands
    int bpf[8] = {3,1,2,0,4,6,5,7}; // array of bytes for i2c reg
    int i=0;
    while ( i<7 && bands[i]!=band) i++;
    wiringPiI2CWriteReg8 (devlna, 9, ((reg&248) + bpf[i] ) );usleep(i2c_delay);
    return i+1; //number of filter BPFn 1-8
  }
/////////////////////// Gain differ-table
  int frqzone=0;
  if (freq>2000000000) frqzone=0;
  else if (freq>1500000000) frqzone=1;
  else frqzone=3;
  struct zone_t { //
    unsigned long min;      //minimal amp in sq
    unsigned long long max; //maximum ampin sq
    int dbshift;   // +dB via ext LNA+ATT &internal(LNA+TIA+PGA) 
    int delta;     //calibrated delta with lowbw.ini
    int delta2;    //calibrating shift without lowbw.ini 
  };
  struct zone_t zone[4][6];
//////////////////////freq 2135
//zone 0;
  zone[0][0].min=100*100;     //+4dB
  zone[0][0].max=30000*30000; //+56dB infinity
  zone[0][0].dbshift=-16;
  zone[0][0].delta=-8;        //9-10
  zone[0][0].delta2=0;
//zone 1;
  zone[0][1].min=185*185;     // -4dB
  zone[0][1].max=1700*1700;   // +15dB
  zone[0][1].dbshift=0;
  zone[0][1].delta=-11;
  zone[0][1].delta2=0;
//zone 2;
  zone[0][2].min=227*227;      //-24dB
  zone[0][2].max=16600*16600;  //+8dB
  zone[0][2].dbshift=24;
  zone[0][2].delta=-11;
  zone[0][2].delta2=1;
//zone 3;
  zone[0][3].min=830*830;        // -36dB
  zone[0][3].max=20000*20000;    // -8dB 
  zone[0][3].dbshift=48;
  zone[0][3].delta=-14;
  zone[0][3].delta2=2;
//zone 4;
  zone[0][4].min=650*650;       //-64dB
  zone[0][4].max=28000*28000;   //-28dB
  zone[0][4].dbshift=72;
  zone[0][4].delta=-15;
  zone[0][4].delta2=1;
//zone 5;
  zone[0][5].min=0;             //-92dB    
  zone[0][5].max=16000*16000;   //-52dB
  zone[0][5].dbshift=88;
  zone[0][5].delta=-12;
  zone[0][5].delta2=1;
////////////////////////////////////   freq 1819.
//zone 0;
  zone[1][0].min=400*400;
  zone[1][0].max=5000*5000;
  zone[1][0].dbshift=-16;
  zone[1][0].delta=-8;          //9-10
  zone[1][0].delta2=-14;
//zone 1;
  zone[1][1].min=200*200;
  zone[1][1].max=10000*10000;
  zone[1][1].dbshift=0;
  zone[1][1].delta=-10;
  zone[1][1].delta2=-15;
//zone 2;
  zone[1][2].min=260*260;
  zone[1][2].max=20000*20000;
  zone[1][2].dbshift=24;
  zone[1][2].delta=-9;
  zone[1][2].delta2=-3;
//zone 3;
  zone[1][3].min=222*222;
  zone[1][3].max=25000*25000;
  zone[1][3].dbshift=48;
  zone[1][3].delta=-12;
  zone[1][3].delta2=-2;
//zone 4;
  zone[1][4].min=1300*1300;
  zone[1][4].max=25000*25000;
  zone[1][4].dbshift=72;
  zone[1][4].delta=-10;
  zone[1][4].delta2=0;
//zone 5;
  zone[1][5].min=10*10;
  zone[1][5].max=19000*19000;
  zone[1][5].dbshift=88;
  zone[1][5].delta=-8;
  zone[1][5].delta2=-1;
//////////////////////////////////////freq 892.4 & 943.0 (850-950)
//zone 0;
  zone[3][0].min=500*500;
  zone[3][0].max=40000*40000;
  zone[3][0].dbshift=-16;
  zone[3][0].delta=10;     //9-10
  zone[3][0].delta2=2;
//zone 1;
  zone[3][1].min=200*200;
  zone[3][1].max=19000*19000;
  zone[3][1].dbshift=0;
  zone[3][1].delta=-18;
  zone[3][1].delta2=-26;
//zone 2;
  zone[3][2].min=132*132;   // -24db
  zone[3][2].max=28000*28000;
 // 0db
  zone[3][2].dbshift=24;
  zone[3][2].delta=-17;
  zone[3][2].delta2=-14;
//zone 3;
  zone[3][3].min=120*120;
  zone[3][3].max=26000*26000;
  zone[3][3].dbshift=48;
  zone[3][3].delta=-19;
  zone[3][3].delta2=-11;
//zone 4;
  zone[3][4].min=1600*1600;    //-51dB
  zone[3][4].max=30000*30000;
  zone[3][4].dbshift=72;
  zone[3][4].delta=-19;
  zone[3][4].delta2=-10;
//zone 5;
  zone[3][5].min=10*10;
  zone[3][5].max=26000*26000;  //-48dB
  zone[3][5].dbshift=88;
  zone[3][5].delta=-11;
  zone[3][5].delta2=-10;

///////////////////////////////////////////////////////////////////////////////
  int setzone(int z){
    if (debug) printf("sz.curzone=%d newzone=%d",curzone,z);
    if (z<0 ||z>5) {printf("!!!!!!!!!!!error!!!!!!!!!!!!!");return -1;}
    if (z==0) {setgain(0); extlnaoff(); extatton();}
    else if (z==5) {setgain(72); extlnaon(); extattoff();} 
    else {setgain(zone[0][z].dbshift); extlnaoff(); extattoff();}
    if (curzone!=z) flagzone = 100;
    return z;
  }
//// Q-mine
  unsigned long qmine(int start,int bufsize){
    unsigned long long max=0;
    unsigned long long skr=0;
    while(!max){
      nb_samples=LMS_RecvStream( &rx_stream, buff, bufsize, NULL, 1000 );
      for (int ii=start; ii<(start+bufsize);ii++){
        skr+=buff[ii].i*buff[ii].i+buff[ii].q*buff[ii].q;
        if ((15&(ii+1))==0) {
          skr=skr>>4; 
          if (max<skr) max=skr;
          skr=0;
        }
      }
    }
    if(max) return max;
    else printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! %d %d\n",bufsize, nb_samples); 
  }
///////////////// L-mine
  unsigned long lmine(){
    nb_samples=LMS_RecvStream( &rx_stream, buff, buffer_size, NULL, 1000 );
    unsigned long max=0;
    unsigned long long skr=0;
 //alg1
    if (alg==1 ||alg==4) return qmine(0,buffer_size);
 //alg2
    if (alg==2){
      /////////////////////// find SNR & middle limit
      int cursize=0,psize=0;
      float snr=0;
      ourpacket=0;
      unsigned long signal=0,noise=40000*40000, middle=0, max=0;
      for (int ii=0; ii<buffer_size;ii++){
        skr+=buff[ii].i*buff[ii].i+buff[ii].q*buff[ii].q;
        if ((15&(ii+1))==0) {
          skr=skr>>4; 
          if (signal<skr) signal=skr;
          if (noise>skr && skr!=0) noise=skr;
          skr=0;
        }
      }
      middle=(signal-noise)>>1;
      if (noise && signal<40000*40000 ) snr=10*log10(signal/noise);
      snr_global=snr;
      if (snr<3) return signal;
  ////// end find snr 
      unsigned long maxlielevel;
      for (int ii=0; ii<buffer_size;ii++){
        skr+=buff[ii].i*buff[ii].i+buff[ii].q*buff[ii].q;
        if ((15&(ii+1))==0) {
          skr=skr>>4; 
          if (maxlielevel<skr) maxlielevel=skr;
          if (skr>middle) cursize++;
          else {
            if (abs(cursize-oursize)<deltablock) { //zamer&return
              ourpacket=1;
//              return qmine(ii-oursize*16,oursize*16);
              for (int i=ii;i>ii-(oursize*16);i--){
                skr=buff[i].i*buff[i].i+buff[i].q*buff[i].q;
                if (max<skr) max=skr;
              }
              return max;
            }
            cursize=0;
          }
          skr=0; 
        }
      }
      return maxlielevel;
    } //end alg2
    if (alg==3){
      /////////////////////// find SNR & middle limit
      int cursize=0,psize=0;
      float snr=0;
      ourpacket=0;
      unsigned long signal=0,noise=40000*40000, middle=0, max=0;
      for (int ii=0; ii<buffer_size;ii++){
        skr+=buff[ii].i*buff[ii].i+buff[ii].q*buff[ii].q;
        if ((15&(ii+1))==0) {
          skr=skr>>4; 
          if (signal<skr) signal=skr;
          if (noise>skr && skr!=0) noise=skr;
          skr=0;
        }
      }
      middle=(signal-noise)>>1;
      if (noise && signal<40000*40000 ) snr=10*log10(signal/noise);
      snr_global=snr;
      if (snr<3) return signal;
  ////// end find snr 
      unsigned long maxlielevel;
      for (int ii=0; ii<buffer_size;ii++){
        skr+=buff[ii].i*buff[ii].i+buff[ii].q*buff[ii].q;
        if ((15&(ii+1))==0) {
          skr=skr>>4; 
          if (maxlielevel<skr) maxlielevel=skr;
          if (skr>middle) cursize++;
          else {
            if (cursize>m3oursize) { //zamer&return
              ourpacket=1;
              return qmine(ii-oursize*16,oursize*16);
//              for (int i=ii;i>ii-(oursize*16);i--){
//                skr=buff[i].i*buff[i].i+buff[i].q*buff[i].q;
//                if (max<skr) max=skr;
//              }
//              return max;
            }
            cursize=0;
          }
          skr=0; 
        }
      }
      return maxlielevel;
    } //end alg3
  }
//////////////////// end L-mine
//////////////////// Auto set level ////////
  int autosetlevel(){
    unsigned long tmpxbl;
    int bufsize=buffer_size;
    if (alg==2 || alg==4 || alg==3) bufsize=160000;
    tmpxbl=qmine(0,bufsize);
    unsigned long xtmpxbl = tmpxbl;
    int errlvl=0;
    while (tmpxbl < zone[frqzone][curzone].min || tmpxbl > zone[frqzone][curzone].max  ){
      errlvl++;
      if (errlvl >3) {if (debug) printf("errorlevel=%d \n",errlvl);return -3;}
      tmpxbl=qmine(0,bufsize);  //mine level
      if (flagzone) {
        unsigned long res = abs(tmpxbl-xtmpxbl);
        if (res>tmpxbl || res>xtmpxbl) {flagzone=0;xtmpxbl=tmpxbl;} else flagzone--;
        continue;
      }
      if (tmpxbl < zone[frqzone][curzone].min ) { //nead ampl.
        if (curzone<5)  {curzone=setzone(curzone+1); tmpxbl=qmine(0,bufsize);} //we can? we do!
        else {tmpxbl=zone[frqzone][curzone].min;return -1;} //else we lost him! 
      } 
      else if (tmpxbl > zone[frqzone][curzone].max ) { //nead att
        if (curzone>0) {curzone=setzone(curzone-1);tmpxbl=qmine(0,bufsize);} //we can? we do!
        else {tmpxbl=zone[frqzone][curzone].max; return -2;};  //else we find him!
      }
//      tmpxbl=qmine(bufsize);                     // <------------------------------------! TEST IT
    } //end while
    return 0;
  } //end auto
///////////////////////////////////// main loop
  short delay=0, delaylimit=2, zerocnt=0, zerolimit=30;  //

  curzone=setzone(5);
  printf("BPF N%d On\n",setband(band));
  if (maxnumber>-1 && debug)  printf("main.alg=%d. Iterations counter : %lld\n",alg,maxnumber);
  else if (debug) printf("main.alg=%d. start infinity loop. \n",alg);

  int out1(){
      if (xmblock>0) {
        bell=log10(round(xmblock)/1000000);
        sbl=round(10*bell);
      } else {sbl=-100;bell=-10;}
        fprintf(stderr, "%d,%d,%d,%d,%d,%d,%d,%.1f\n",
          (int)round(sqrt(xmblock)),
          sbl-zone[frqzone][curzone].dbshift-zone[frqzone][curzone].delta,
          zone[frqzone][curzone].dbshift,
          sbl,
          curzone,
          frqzone,
          delay,
          10*bell-zone[frqzone][curzone].dbshift-zone[frqzone][curzone].delta);
  }
  int out21(){
          fprintf(stderr, "%d,%d,%d,%d,%d,%d,%d,%d,%.1f,%d,%.1f\n",        // out1
            (int)round(sqrt(xmblock)),
            sbl-zone[frqzone][curzone].dbshift-zone[frqzone][curzone].delta, 
            zone[frqzone][curzone].dbshift,
            sbl,
            curzone,
            frqzone,
            delay,
            isAlexIni,
            snr_global,
            ourpacket,
            10*bell-zone[frqzone][curzone].dbshift-zone[frqzone][curzone].delta);
  }
  int out20(){
    fprintf(stderr, "%d,%d,%d,%d,%d,%d,%d,%.1f,%d,%.1f\n",   // out2
     (int)round(sqrt(xmblock)),
     -96, 
     zone[frqzone][curzone].dbshift, 
     sbl,
     curzone,
     frqzone,
     delay,
     snr_global,
     ourpacket,
     -96.01);
  }
////////////////////////////                ///////////
  fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);
  char sbuf[2];
  short started = 0;
//int checkr() {
//    char buf[1];
//    int xxread = read(0,sbuf,1);
//    return atoi(sbuf);
//}
//double maxtime=0;
  while( maxnumber--!=0 ){ //start main loop
//    start=clock();
//    if (maxnumber==-10) maxtime=0;
    if (alg==5){
     ///transfer buffer 2 file@tmpfs
      fd = fopen("/dev/shm/iq1","wb");
      nb_samples=LMS_RecvStream( &rx_stream, buff, buffer_size, NULL, 1000 );
      fwrite( buff, sizeof( *buff ), nb_samples,fd);
      fclose(fd);
      if (rename("/dev/shm/iq1","/dev/shm/iq")) fprintf(stderr,"Alarm");
     /// read from client active zone
      int xxread = read(0,sbuf,2);
	if (xxread>0) {int i = atoi(sbuf);
      if (i>0 && i <7) {curzone = setzone(i-1); i=0;fprintf(stderr,"ZONE-%d\n",curzone);}
	} 
      if (!started) {started = 1; fprintf(stderr,"getready\n");}//expot once "getready"
    }
    else if (alg==1 || alg==4) {   // for metod 1 or 4
      xmblock = qmine(0,5120);  //// test 4 nead autosetlelvel:
      if (xmblock > zone[frqzone][curzone].min && xmblock < zone[frqzone][curzone].max) delay=0; 
      else {
        delay++;
        xmblock=prevxmblock;
        if (delay >= delaylimit){delay=0;autosetlevel();}
      } ///// end test
      prevxmblock=xmblock;
      xmblock=lmine();
      out1();

    } else if (alg==2 || alg==3) { // start metod 2
      xmblock = lmine();
      if (!ourpacket) zerocnt++; else zerocnt=0;
      if ((zerocnt<zerolimit && xmblock<zone[frqzone][curzone].min) || (xmblock > zone[frqzone][curzone].min && xmblock < zone[frqzone][curzone].max))  {
        if (ourpacket) {
          bell=log10(round(xmblock)/1000000);
          sbl=round(10*bell);
          if (!isAlexIni) sbl+=zone[frqzone][curzone].delta2;
          out21();
        } else out20();
      //end out
      } else {
        zerocnt=0;
        autosetlevel();
        out20();
      }
    }
    if (curzone<0 || curzone>5) {printf("!!!!!curzone=%d\n",curzone);return -1; } //test for past edition
  } /////////////////////// end mainloop /////////////////
  LMS_StopStream(&rx_stream);
  LMS_DestroyStream(device, &rx_stream);
  free( buff );
  if (alg!=5) fclose(fd);
  LMS_Close(device);
  printf("\nsamplerate %d\nbuffer_size=%d\n",(int)sample_rate,(int)buffer_size);
  printf("version %s\n",version);
  return 0;
}
