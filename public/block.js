
    function change(){
      var chnumber=document.getElementById("chnumber");
      var diapaz=document.getElementById("diapaz");
      if (diapaz.value=='1' ) chnumber.value='номер канала 128...251';
      if (diapaz.value=='2' ) chnumber.value='номер канала 1...124';
      if (diapaz.value=='3' ) chnumber.value='номер канала 0...124 975-1023';
      if (diapaz.value=='4' ) chnumber.value='номер канала 0...124 955-1023';
      if (diapaz.value=='5' ) chnumber.value='номер канала 512...885';
      if (diapaz.value=='6' ) chnumber.value='номер канала 512...810';
      if (diapaz.value=='7' ) chnumber.value='UARFCN';
      return chnumber.value;
    }
        
    function zout(){

      var chnumber=document.getElementById("chnumber");
      var diapaz=document.getElementById("diapaz");
      var dnk=document.getElementById("downlink");
      var unk=document.getElementById("uplink");
      var n=chnumber.value;

/////////////// GSM

      if (diapaz.value=='1' ){
        if (n <128 || n >251) {change(); return false;}
        else {unk.value=(8242+(n-128)*2)/10;dnk.value = 1*unk.value + 45;}
      }
      if (diapaz.value=='2' ){
        if (n <1 || n >124) {change(); return false;}
        else { unk.value = (8900 + 2 * n)/10; dnk.value = 1*unk.value + 45;}
      }
      if (diapaz.value=='3' ){
        if ((n > 124 && n <975) || n > 1023 || n < 0) {change(); return false;}
        if (n >= 0 && n <=124) { unk.value = ((2*n)+8900)/10; dnk.value = 1*unk.value + 45;}
        if (n >= 975 && n <=1023) {unk.value =((2*(n-1024)) + 8900)/10; dnk.value = 1*unk.value + 45;}
      }
      if (diapaz.value=='4' ){
        if ((n > 124 && n <955) || n > 1023 || n < 0) {change(); return false;}
        if (n >= 0 && n <=124) { unk.value = ((2*n)+8900)/10; dnk.value = 1*unk.value + 45;}
        if (n >= 955 && n <=1023) {unk.value =((2*(n-1024)) + 8900)/10; dnk.value = 1*unk.value + 45;}
      }
      if (diapaz.value=='5' ){
        if (n <512 || n >885) {change(); return false;}
        else { unk.value = (17102 + 2 * (n - 512))/10; dnk.value = 95 + 1 * unk.value;}
      }        
      if (diapaz.value=='6' ){
        if (n <512 || n >810) {change(); return false;}
        else { unk.value = (18502 + 2 * (n - 512))/10; dnk.value = 80 + 1 * unk.value;}
      }
      if (diapaz.value=='7' ) calc(n);


      unk.value = unk.value*1000000;
      dnk.value = dnk.value*1000000;

      var downch=document.getElementById("downch");
      downch.value=0;

      return true;
      
    }//end of function out()

//// UMTS
    function calc(Ndl){
      if (((Ndl >= 10562) && (Ndl <=10838))) {FDL_offset = 0;	FUL_offset = 0 ; diff = Ndl-10562;	Nul = 9612+diff;	}
      else if (((Ndl >= 9662) && (Ndl <=9938))) {	FDL_offset = 0;	    FUL_offset = 0 ;	  diff = Ndl-9662;	Nul = 9262+diff;	}
      else if (((Ndl >= 1162) && (Ndl <=1513)))	{	FDL_offset = 1575;	FUL_offset = 1525 ; diff = Ndl-1162;	Nul = 937+diff;	  }
      else if (((Ndl >= 1537) && (Ndl <=1738)))	{	FDL_offset = 1805;	FUL_offset = 1450 ;	diff = Ndl-1537;	Nul = 1312+diff;	}
      else if (((Ndl >= 4357) && (Ndl <=4458)))	{	FDL_offset = 0; 		FUL_offset = 0 ;		diff = Ndl-4357;	Nul = 4132+diff;	}
      else if (((Ndl >= 4387) && (Ndl <=4413)))	{	FDL_offset = 0; 		FUL_offset = 0 ;		diff = Ndl-4387;	Nul = 4162+diff;	}
      else if (((Ndl >= 2237) && (Ndl <=2563))) {	FDL_offset = 2175;	FUL_offset = 2100 ;	diff = Ndl-2237;	Nul = 2012+diff;	}
      else if (((Ndl >= 2937) && (Ndl <=3088)))	{	FDL_offset = 340;		FUL_offset = 340 ;	diff = Ndl-2937;	Nul = 2712+diff;	}
      else if (((Ndl >= 9237) && (Ndl <=9387)))	{	FDL_offset = 0; 		FUL_offset = 0 ;		diff = Ndl-9237;	Nul = 8762+diff;	}
      else if (((Ndl >= 3112) && (Ndl <=3388)))	{	FDL_offset = 1490;	FUL_offset = 1135 ;	diff = Ndl-3112;	Nul = 2887+diff;	}
      else if (((Ndl >= 3712) && (Ndl <=3787)))	{	FDL_offset = 736;		FUL_offset = 733 ;	diff = Ndl-3712;	Nul = 3487+diff;	}
      else if (((Ndl >= 3842) && (Ndl <=3903)))	{	FDL_offset = -37;		FUL_offset = -22 ;	diff = Ndl-3842;	Nul = 3617+diff;	}
      else if (((Ndl >= 4017) && (Ndl <=4043)))	{	FDL_offset = -55 ;	FUL_offset = 21 ; 	diff = Ndl-4017;	Nul = 3792+diff;	}
      else if (((Ndl >= 4117) && (Ndl <=4143)))	{	FDL_offset = -63;		FUL_offset = 12 ;		diff = Ndl-4117;	Nul = 3892+diff;	}
      else if (((Ndl >= 712) && (Ndl <=763)))		{	FDL_offset = 735;		FUL_offset = 770 ;	diff = Ndl-712;		Nul = 312+diff;		}
      else if (((Ndl >= 4512) && (Ndl <=4638)))	{	FDL_offset = -109;	FUL_offset = -23 ;	diff = Ndl-4512;	Nul = 4287+diff;	}
      else if(((Ndl >= 862) && (Ndl <=912)))		{	FDL_offset = 1326;	FUL_offset = 1358 ;	diff = Ndl-862;		Nul = 462+diff;		}
      else if (((Ndl >= 4662) && (Ndl <=5038)))	{	FDL_offset = 2580;	FUL_offset = 2525 ;	diff = Ndl-4662;	Nul = 4437+diff;	}
      else if (((Ndl >= 5112) && (Ndl <=5413))) {	FDL_offset = 910;		FUL_offset = 875 ;	diff = Ndl-5112;	Nul = 4887+diff;	}
      else if (((Ndl >= 5762) && (Ndl <=5913)))	{	FDL_offset = -291;	FUL_offset = -291 ;	diff = Ndl-5762;	Nul = 5537+diff;	}
      else{
        FDL_offset = 0;
        FUL_offset = 0;
        diff = 0; 
        Nul = 0; 
        change();
        return false;
        }
      var downch=document.getElementById("downch");
      var dnk=document.getElementById("downlink");
      var unk=document.getElementById("uplink");   
      dnk.value=((FDL_offset + (2 * Ndl)/10));
      unk.value=((FUL_offset + (2 * Nul)/10));
      downch.value=Nul;
      return true;
    }

//
    function my_onclick_script(){} // HAXPEH ESLI nenuzhna



// --------------------------------------------------------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@




 var mezhdupalkami=2;

 var array1 = [];
 var maini = [];
 var globwidth=window.innerWidth;   // установка ширины мвссива и вывода
 var arraysize=Math.round(globwidth/mezhdupalkami);
 var tmpout1='';

function dec2hex(n){
    n = parseInt(n); var c = 'ABCDEF';
    var b = n / 16; var r = n % 16; b = b-(r/16); 
    b = ((b>=0) && (b<=9)) ? b : c.charAt(b-10);    
    return ((r>=0) && (r<=9)) ? b+''+r : b+''+c.charAt(r-10);
}

// rndcreate();  //забивка нулями исходя из размера  можно убрать
 function rndcreate(){
   var i=arraysize;
   while (i-- > 0 ) array1[i] = Math.round(Math.random()*100); //если не нулями а случайными
 }

 function newelm(elm){  // добовляем новый элемент в массив и перестраеваем массив
   var i=0;
   while (i++ < arraysize ) array1[i] = array1[i+1];
   array1[arraysize] = elm;
 } 

 function scale(canid){           //  graduirovka клеточками можно убрать
   var xstep = 10; // размер клетки по Х
   var ystep = 10; // и по У
   var canvas = document.getElementById(canid);
   var context = canvas.getContext("2d");
      for (var x = 0.5; x < canvas.width; x += xstep) {
    context.beginPath();
    context.moveTo(x, 0);
    context.lineTo(x, canvas.height);
    context.strokeStyle = "#ffeeee";
    context.stroke();
   } 
   for (var y = 0.5; y < canvas.height; y += ystep) {
    context.beginPath();       
    context.moveTo(0, y);
    context.lineTo(canvas.width, y);
    context.strokeStyle = "eeeeee";
    context.stroke();
   }
 }
 xxnow = 0;
 function clsscale(canid){      // очистка канваса
   var canvas = document.getElementById(canid);
   var context = canvas.getContext("2d");
   context.setTransform(1, 0, 0, 1, 0, 0);
   context.clearRect(0, 0, canvas.width, canvas.height);
  }
  
 function xout(canid,size){           //вывод на канвас массива
   var canvas = document.getElementById(canid);
   var context = canvas.getContext("2d");
   canvas.width  = globwidth;
   if (canvas.height > window.innerHeight) canvas.height = window.innerHeight; //убрать потом
   clsscale(canid);
//   scale(canid); // вот этот вызов прорисовки клеток можно убрать тут и клеток не будет
   var x=0;
   var counter=0;//13.4
   var maxcount=0;

    context.beginPath();
     context.moveTo(0, canvas.height-140);
     context.lineTo(canvas.width, canvas.height-140);
     context.strokeStyle = "#FFFFFF";
     context.stroke();

   while (x < canvas.width){     
     var colorstring="";
     x=x+size; 
     var ztmp = array1[counter]/8;
     context.beginPath();
     context.moveTo(x, canvas.height-140);
     context.lineTo(x, canvas.height - ztmp -140);
//     colorstring="#FF" + dec2hex(255-ztmp) +  dec2hex(255-ztmp);
     colorstring="#FF0000";
     if (ztmp>=0) colorstring="#0000BB";
     context.strokeStyle = colorstring;
     context.stroke();
     context.beginPath();
     context.arc(x, canvas.height - ztmp -140, 2, 0, 2*Math.PI, false);
     context.strokeStyle = "#FFFFFF";
     context.stroke();
     if (maxcount < ztmp) maxcount = ztmp;
     counter++;     
   }
//out maxline

   context.beginPath();
   context.moveTo(0, canvas.height - maxcount -140);
   context.lineTo(canvas.width, canvas.height -maxcount -140);
   context.strokeStyle = "#FF4422";
   context.moveTo(50, canvas.height - maxcount-140); 
   context.font = "24px Tahoma";
   context.fillStyle = "#cccccc";
   context.fillText(maxcount*8, 320, canvas.height - maxcount- 2 - 140 );
   context.stroke();

//out curline
   context.beginPath();
   context.moveTo(0, canvas.height - ztmp - 140);
   context.lineTo(canvas.width, canvas.height -ztmp -140);
   context.strokeStyle = "#FFFFFF";
   context.fillStyle = "#FFFFFF";
   context.font = "28px arial";
   if (ztmp>0) tmpout1=(ztmp*8)+" dB";
   context.fillText(tmpout1, 80, canvas.height - ztmp -2 -140 );
   context.stroke();
 }

 function newn(n){  //функция добавляет к массиву новый элемент и выводит канвас на экран

//window.status = n;
//	n = n+100;
     xxnow = n;
//	if (n>0) alert(n);
//     if (n<0) n*=-1;
     newelm(n);
     xout('outbox',mezhdupalkami); 
 }


